# S02 Activity:

# 1. Get a year from the user and determine if it is a leap year or not.

year = int(input("Enter a year: "))

#
if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
    print(f"{year} is a leap year.")
else:
    print(f"{year} is not a leap year.")



# 2.  Get two numbers (row and col) from the user and create by row x col grid of asterisks given the two numbers.

rows = int(input("Enter the number of rows: "))
cols = int(input("Enter the number of columns: "))

#
for i in range(rows):
    for j in range(cols):
        print("*", end=" ")
    print()




# Stretch goal: Implement error checking for the leap year input.

# 1. Strings are not allowed for inputs.

while True:
    try:
        rows = int(input("Enter the number of rows: "))
        break
    except ValueError:
        print("Please enter a valid integer for rows.")

#
while True:
    try:
        cols = int(input("Enter the number of columns: "))
        break
    except ValueError:
        print("Please enter a valid integer for columns.")

#
for i in range(rows):
    for j in range(cols):
        print("*", end=" ")
    print()


# 2. No zero or negative values.

while True:
    try:
        rows = int(input("Enter the number of rows: "))
        if rows > 0:
            break
        else:
            print("Please enter a positive integer for rows.")
    except ValueError:
        print("Please enter a valid positive integer for rows.")

#
while True:
    try:
        cols = int(input("Enter the number of columns: "))
        if cols > 0:
            break
        else:
            print("Please enter a positive integer for columns.")
    except ValueError:
        print("Please enter a valid positive integer for columns.")

#
for i in range(rows):
    for j in range(cols):
        print("*", end=" ")
    print()